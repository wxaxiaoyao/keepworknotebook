## mysql 

### install 
```bash
apt-get install mariadb-server mariadb-client libmysqlclient-dev  # 安装
service mysql restart       # 重启
mysqladmin -u root password "new_password"; 
GRANT ALL PRIVILEGES ON *.* TO 'wuxiangan'@'%' IDENTIFIED BY 'wuxiangan' WITH GRANT OPTION; # 客户端登录
vim /etc/mysql/mariadb.conf.d/50-server.cnf # 屏蔽bind-address=127.0.01行, 允许客户端连接 
```


### 表中文
* 方法一:
创建表时设置字符集 character set = utf8
	```
	create table user (
	  user_id bigint
	) character set = utf8;
	```
* 方法二
`alter table table_name convert to character set utf8;`