
        location / { 
            #root   html;
            #index  index.html index.htm;
            rewrite_by_lua_file /nginx/lua/devGitlabLogin.lua;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_buffering             off;
            proxy_redirect              off;
            proxy_connect_timeout       10; 
            proxy_send_timeout          30; 
            proxy_read_timeout          30; 
            proxy_pass http://121.14.117.253;

        }  